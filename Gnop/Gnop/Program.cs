using System;

namespace Gnop
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Gnop game = new Gnop())
            {
                game.Run();
            }
        }
    }
#endif
}

