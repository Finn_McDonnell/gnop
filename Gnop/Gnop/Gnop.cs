using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XMLLoader;

namespace Gnop
{
    public class Gnop : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        KeyboardState oldState;
        KeyboardState newState;
        List<GameCharacter> players = new List<GameCharacter>();
        Player player;
        Bot enemy;

        BotRoster balls;

        List<SimpleLevel> levelList = new List<SimpleLevel>();
        LevelList stageList;
        SimpleLevel currentStage;
        int currentStageIndex = 0;

        Hashtable botTextures = new Hashtable();
        Texture2D paddleBlue;
        Texture2D paddleRed;
        Texture2D wallTexture;
        Texture2D floorTexture;
        Texture2D ballTexture;

        MenuComponent menu;

        public enum GameState
        {
            Paused,
            Play,
            Menu,
            Win,
            Lose,
            WinRound
        }
        GameState currentState = GameState.Paused;


        public Gnop()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            this.graphics.PreferredBackBufferWidth = 800;
            this.graphics.PreferredBackBufferHeight = 600;
        }

        protected override void Initialize()
        {


            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            //Textures
            paddleBlue = Content.Load<Texture2D>("paddleBlu");
            paddleRed = Content.Load<Texture2D>("paddleRed");
            wallTexture = Content.Load<Texture2D>("Walltile");
            floorTexture = Content.Load<Texture2D>("Floortile");
            ballTexture = Content.Load<Texture2D>("ballGrey");
            botTextures.Add("ballGrey", ballTexture); //The game library support multiple enemy types, each of which can be given a distinct texture. This can be used to introduce new features later.

            //Fonts
            font = Content.Load<SpriteFont>("HUDFont");

            //Menu
            List<String> menuItems = new List<string>();
            menuItems.Add("Exit");
            menuItems.Add("Restart");
            menu = new MenuComponent(this, spriteBatch, font, menuItems);
            Components.Add(menu);

            //Player
            player = new Player(GraphicsDevice, paddleBlue, 0.80f, 0, TimeSpan.Zero, 5, TimeSpan.Zero, true);
            player.setStartPosition(100, 350);
            player.calculateLocalAxes(); //Ensure the object has update local axes for determining reflection directions.
            players.Add((GameCharacter) player);

            enemy = new Bot(GraphicsDevice, paddleRed, 0.4f, 0, TimeSpan.Zero, 5, true);
            enemy.setStartPosition(700,350);
            enemy.calculateLocalAxes();
            players.Add((GameCharacter)enemy);


            //Level
            stageList = Content.Load<LevelList>("Stages");
            foreach (string stage in stageList.Levels)
            {
                SimpleLevel theLevel = new SimpleLevel(Content.Load<CharacterList>(stage));
                levelList.Add(theLevel);
            }
            currentStage = levelList[currentStageIndex];
            initializeCurrentLevel();
        }

        private void initializeCurrentLevel() //Defer level initialisation for genericity
        {
            if (currentStage.Initialized)
            {
                resetLevel();
                balls = currentStage.Roster;
            }
            else
            {
                currentStage.InitialiseWalls(GraphicsDevice, Window.ClientBounds.Height, Window.ClientBounds.Width);
                currentStage.InitialiseBots(GraphicsDevice, player, botTextures); //Initialise balls for this level
                balls = currentStage.Roster;
                enemy.resetToStart();
                

                currentStage.BackDrop.Texture = floorTexture;
                foreach (DictionaryEntry entry in currentStage.Walls)
                {
                    GameObject wall = entry.Value as GameObject;
                    wall.Texture = wallTexture;
                }
            }
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            UpdateInput(player, gameTime);

            switch (currentState)
            {
                case GameState.Paused:
                    break;
                case GameState.Play:
                    checkForGameEnd();
                    player.Update(gameTime.ElapsedGameTime);
                    moveTowardBall(enemy, findClosestBall(enemy));
                    enemy.Update(gameTime.ElapsedGameTime);
                    resolvePaddleWallCollisions((GameCharacter)player);
                    resolvePaddleWallCollisions((GameCharacter)enemy);
                    foreach (Bot ball in balls.Roster)
                    {
                        resolveBallWallCollisions((GameCharacter)ball);
                        resolveBallPaddleCollisions((GameCharacter)ball);
                    }
                    balls.UpdateBots(gameTime.ElapsedGameTime);

                    break;
                case GameState.Win:
                    break;
            }

            base.Update(gameTime);
        }

        private void UpdateInput(Player player, GameTime gameTime) //Determine relevant keystrokes made and keys that are held down
        {
            newState = Keyboard.GetState();

            bool up = newState.IsKeyDown(Keys.Up);
            bool down = newState.IsKeyDown(Keys.Down);
            bool turbo = false; //The game libraries support some extra basic functions for user playable characters,
            bool shoot = false; //these could be used to introduce new features to this game.
            bool left = false;
            bool right = false;
            player.updatePlayerInput(left, right, up, down, turbo, shoot, gameTime); //Pass player relevant key strokes on for handling
            pauseHandle(); //Defer keystroke handling for pause state to another method
            menuHandle(); //Defer keystroke handling for Menu state to another method
            winLoseHandle(); //Defer keystroke handling for Win/Lose state to another method
            oldState = newState; //Take care of caching keyboard state to allow button press/depress recognition
        }

        private void pauseHandle() //keystroke handling for pause state
        {
            if (currentState == GameState.Paused & checkKeyPress(Keys.Enter))
            {
                currentState = GameState.Play;
            }
            else if (currentState == GameState.Play & checkKeyPress(Keys.Enter)) currentState = GameState.Paused;
            else if (currentState == GameState.WinRound & checkKeyPress(Keys.Enter))
            {
                loadNextStage();
                currentState = GameState.Paused;
            }
        }
        private void winLoseHandle()
        {
            if ((currentState == GameState.Win | currentState == GameState.Lose) & checkKeyPress(Keys.Enter)) currentState = GameState.Menu;
        }

        private void menuHandle() //keystroke handling for menu state
        {
            if (currentState == GameState.Menu & checkKeyPress(Keys.Enter))
            {
                if (menu.SelectedIndex == 0) this.Exit();
                if (menu.SelectedIndex == 1) initializeCurrentLevel();
            }
            else if (currentState == GameState.Play & checkKeyPress(Keys.Escape)) currentState = GameState.Menu;
            else if (currentState == GameState.Menu & checkKeyPress(Keys.Escape)) currentState = GameState.Play;
        }

        private void loadNextStage() //Increment and initialise the new stage
        {
            if (currentStageIndex < levelList.Count)
            {
                currentStageIndex++;
                currentStage = levelList[currentStageIndex];
                initializeCurrentLevel();
            }
        }

        private void resetLevel()
        {
            foreach (Bot bot in balls.Roster)
            {
                bot.resetToStart();
            }

            currentStageIndex = 0;
            currentStage = levelList[currentStageIndex];

            player.resetToStart();
            enemy.resetToStart();

            currentState = GameState.Paused;
        }

        private void resetBalls()
        {
            foreach (Bot bot in balls.Roster)
            {
                bot.resetToStart();
                bot.speed = bot.InitialSpeed;
            }

            currentState = GameState.Paused;
        }


        private void updateHUD()
        {
            Vector2 playerScoreLocation = new Vector2();
            playerScoreLocation.X = 10;
            playerScoreLocation.Y = 10;
            spriteBatch.DrawString(font, player.Toughness.ToString(), playerScoreLocation, Color.White);

            Vector2 enemyScoreLocation = new Vector2();
            enemyScoreLocation.X = Window.ClientBounds.Width - 40;
            enemyScoreLocation.Y = 10;
            spriteBatch.DrawString(font, enemy.Toughness.ToString(), enemyScoreLocation, Color.White);

        } //Update scores

        private void checkForGameEnd() //Checks to see if either player has lost
        {
            if (!enemy.Active)
            {
                if (currentStageIndex == levelList.Count - 1) currentState = GameState.Win;
                else if (currentStageIndex < levelList.Count) currentState = GameState.WinRound;
            }
            else
            {
                if (!player.Active) currentState = GameState.Lose;
                else
                {
                    checkForRoundEnd();
                }
            }
        }

        private void checkForRoundEnd() //Checks if all balls are inactive, then a reset needs to happen
        {
                bool result = true;

                foreach (Bot bot in balls.Roster)
                {
                    if (bot.Active)
                    {
                        result = false;
                        break;
                    }
                }

                if (result)
                {
                    resetBalls();
                    currentState = GameState.Paused;
                }


        }

        public bool checkKeyPress(Keys key)
        {
            return newState.IsKeyUp(key) && oldState.IsKeyDown(key);
        } //Checks if a key was pressed within the last frame

        private void resolvePaddleWallCollisions(GameCharacter character) //Check for Paddle collisions with game boundaries and block character from leaving screen
        {
            int collidedX = (int)character.Position.X;
            int collidedY = (int)character.Position.Y;

            foreach (DictionaryEntry entry in currentStage.Walls)
            {
                GameObject obstacle = entry.Value as GameObject;

                if ((string)entry.Key == "top" & character.boundingBox.Intersects(obstacle.boundingBox))
                {
                    collidedY = (int)(obstacle.boundingBox.Height + character.Origin.Y * character.Scale.Y);
                    character.Velocity.Y = 0.0f;
                }
                if ((string)entry.Key == "bottom" & character.boundingBox.Intersects(obstacle.boundingBox))
                {
                    collidedY = (int)(obstacle.boundingBox.Y - character.Origin.Y * character.Scale.Y);
                    character.Velocity.Y = 0.0f;
                }
                if ((string)entry.Key == "left" & character.boundingBox.Intersects(obstacle.boundingBox))
                {
                    collidedX = (int)(obstacle.boundingBox.Width + character.Origin.X);
                    character.Velocity.X = 0.0f;
                }
                if ((string)entry.Key == "right" & character.boundingBox.Intersects(obstacle.boundingBox))
                {
                    collidedX = (int)(obstacle.boundingBox.X - character.Origin.X);
                    character.Velocity.X = 0.0f;
                }

                character.setPosition(collidedX, collidedY);
            }
        }        

        private void resolveBallWallCollisions(GameCharacter character) //Check for Ball collisions with game boundaries and block character from leaving screen
        {
            if (character.Active)
            {
                foreach (DictionaryEntry entry in currentStage.Walls)
                {
                    GameObject obstacle = entry.Value as GameObject;

                    if (((string)entry.Key == "top") & character.boundingBox.Intersects(obstacle.boundingBox))
                    {
                        character.Velocity.Y = Math.Abs(character.Velocity.Y);
                    }
                    if (((string)entry.Key == "bottom") & character.boundingBox.Intersects(obstacle.boundingBox))
                    {
                        character.Velocity.Y = -Math.Abs(character.Velocity.Y);
                    }
                    if (((string)entry.Key == "left") & character.boundingBox.Intersects(obstacle.boundingBox))
                    {
                        character.Velocity.X *= -1;
                        player.Wounded();
                        character.Wounded();
                    }
                    if (((string)entry.Key == "right") & character.boundingBox.Intersects(obstacle.boundingBox))
                    {
                        character.Velocity.X *= -1;
                        enemy.Wounded();
                        character.Wounded();
                    }

                    character.UpdateMovementDirection();
                }
            }
        }

        private void moveTowardBall(GameCharacter character, Bot ball)
        {

            Single YMovementdirection = (ball.Position.Y - character.Position.Y) / Math.Abs((ball.Position.Y - character.Position.Y));
            character.setMovementDirection((int)character.movementDirection.X, (int) YMovementdirection);
            character.UpdateVelocity();
        }
        
        private Bot findClosestBall(GameCharacter player)
        {
            Single distance = 0.0f;

            Bot closest = balls.Roster[0];

            foreach (Bot ball in balls.Roster)
            {
                if (ball.Active) closest = ball;
            }
            
            Single shortestDistance =  Vector2.Distance(closest.Position, player.Position);

            foreach (Bot ball in balls.Roster)
            {
                if (ball.Active)
                {
                    distance = Vector2.Distance(ball.Position, player.Position);
                    if (distance < shortestDistance)
                    {
                        shortestDistance = distance;
                        closest = ball;
                    }
                }
            }
            return closest;
        }

        /* Collisions between 2 moving objects can result in unwanted side effects. Two bodies may intersect in 1 frame, triggering a collison,
         * but if the ball reflection direction is similar to the direction of the paddle a second intersection in the subsequent
         * frame is likely. This triggers another collision.
         * To handle this the section of the paddle the collision occurs with is deduced from its local axes, and the reflection direction
         * explicitly deduced, rather than simply reflecting the balls direction in an axes.
         */
        private void resolveBallPaddleCollisions(GameCharacter character) //Check for Ball collisions with paddle and reflect ball
        {
            foreach (GameCharacter player in players)
            {
                if (character.boundingBox.Intersects(player.boundingBox))
                {
                    Vector2 playerCentre = new Vector2(player.boundingBox.Center.X, player.boundingBox.Center.Y);
                    Vector2 relativePosition = Vector2.Subtract(character.Position, playerCentre);

                    float crossWithc1 = relativePosition.X * player.localAxes[0].Y - relativePosition.Y * player.localAxes[0].X;
                    float crossWithc2 = relativePosition.X * player.localAxes[1].Y - relativePosition.Y * player.localAxes[1].X;

                    if (crossWithc1 * crossWithc2 <= 0)
                    {
                        character.Velocity.X = Math.Abs(character.Velocity.X) * relativePosition.X / Math.Abs(relativePosition.X); // Don't just reverse direction, explicitly move away from collision
                        character.UpdateMovementDirection();
                    }
                    else if (crossWithc1 * crossWithc2 > 0)
                    {
                        character.Velocity.Y = Math.Abs(character.Velocity.Y) * relativePosition.Y / Math.Abs(relativePosition.Y); // Don't just reverse direction, explicitly move away from collision
                        character.UpdateMovementDirection();
                    }

                    if (character.speed <= 2.5f) character.speed *= 1.020f; // Step up speed
                }
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.LinearWrap, DepthStencilState.Default, RasterizerState.CullNone);
            currentStage.RenderWalls(spriteBatch);

            switch (currentState)
            {
                case GameState.Paused:
                    drawPausedState("Ready?");
                    balls.DrawBots(spriteBatch);
                    player.DrawCharacter(spriteBatch);
                    enemy.DrawCharacter(spriteBatch);
                    updateHUD();
                    break;
                case GameState.Play:
                    balls.DrawBots(spriteBatch);
                    player.DrawCharacter(spriteBatch);
                    enemy.DrawCharacter(spriteBatch);
                    updateHUD();
                    break;
                case GameState.Menu:
                    base.Draw(gameTime);
                    break;
                case GameState.Win:
                    drawPausedState("YOU WIN!");
                    break;
                case GameState.Lose:
                    drawPausedState("you lose...");
                    break;
                case GameState.WinRound:
                    drawPausedState("You won the round! Ready for the next?");
                    break;
            }
            spriteBatch.End();
        }

        private void drawPausedState(string message)
        {
            Vector2 location = new Vector2();
            location.X = ((Window.ClientBounds.Width - font.MeasureString(message).X) / 2);
            location.Y = ((Window.ClientBounds.Height - font.MeasureString(message).Y) / 2);
            spriteBatch.DrawString(font, message, location, Color.White);
        }
    }
}
