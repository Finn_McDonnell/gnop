﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

// Very simple class consisting of a list of levels.
// This is to be used in conjunction with Levels to allow
// easy construction of level structure in simple games.  
namespace XMLLoader
{
    public class LevelList
    {
        public string Name;
        public List<String> Levels;
    }
}
