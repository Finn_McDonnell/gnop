﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Gnop
{
    public class BotRoster // a list of bots and handlers for updating and rendering them en masse
    {
        //Members
        public List<Bot> Roster;

        //Constructors
        public BotRoster(List<Bot> bots)
        {
            Roster = bots;
        }

        //Methods
        public void RefreshBots(GameCharacter target) //Run through all bots and maintain their directions
        {
            foreach (Bot enemy in Roster)
            {
                enemy.Active = true;
                enemy.updateOrigin();
                enemy.facePoint(target.Position);
            }
        }

        public void UpdateBots(Vector2 target, TimeSpan dt) //Update bots for main frame cycle, renews direction based on target.
        {
            foreach (Bot enemy in Roster)
            {
                enemy.updateBotDirection(target);
                enemy.Update(dt);
                enemy.facePoint(target);
                enemy.Shoot();
            }
        }

        public void UpdateBots(TimeSpan dt) //Update bots for main frame cycle on current movement direction.
        {
            foreach (Bot enemy in Roster)
            {
                enemy.Update(dt);
                enemy.Shoot();
            }
        }

        public void DrawBots(SpriteBatch spriteBatch) //Run through all bots and draw
        {
            foreach (Bot enemy in Roster)
            {
                enemy.DrawCharacter(spriteBatch);
            }
        }

        public void ResetBotsToStart(Player player) //Run through all bots and reset them to start locations
        {
            foreach (Bot enemy in Roster)
            {
                enemy.resetToStart();
                RefreshBots(player);
            }
        }
    }
}

