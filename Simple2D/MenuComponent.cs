﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Gnop
{
    public class MenuComponent : Microsoft.Xna.Framework.DrawableGameComponent
    {
        //Members
        List<string> menuItems;
        int selectedIndex;
        Vector2 position;
        Vector2 scale;

        Color unselectedColor = Color.White;
        Color selectedColor = Color.Yellow;

        KeyboardState oldKeyState;
        KeyboardState newKeyState;

        SpriteFont font;
        SpriteBatch spriteBatch;

        //Constructors
        public MenuComponent(Game game, SpriteBatch spriteBatch, SpriteFont spriteFont, List<string> menuItems)
            : base(game)
        {
            this.spriteBatch = spriteBatch;
            font = spriteFont;
            this.menuItems = menuItems;
            calcMenuSize();
        }

        //Accessors
        public int SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
            set
            {
                if (value < 0) selectedIndex = menuItems.Count - 1;
                else if (value >= menuItems.Count) selectedIndex = 0;
                else selectedIndex = value;
            }
        }

        //Methods
        private void calcMenuSize() //Calculate the menu layout and location on screen based on contents
        {
            foreach (string menuItem in menuItems)
            {
                Vector2 itemSize = font.MeasureString(menuItem);
                if (itemSize.X > scale.X)
                {
                    scale.X = itemSize.X;
                    scale.Y += font.LineSpacing + 5.0f;
                }
            }
            position = new Vector2(((Game.Window.ClientBounds.Width - scale.Y) / 2), (Game.Window.ClientBounds.Height - scale.Y) / 2);
        }

        public bool checkKeyPress(Keys key) //Check if key was released in last frame
        {
            return newKeyState.IsKeyUp(key) && oldKeyState.IsKeyDown(key);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            newKeyState = Keyboard.GetState();

            if (checkKeyPress(Keys.Down)) ++SelectedIndex;
            if (checkKeyPress(Keys.Up)) --SelectedIndex;

            base.Update(gameTime);

            oldKeyState = newKeyState;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            Vector2 location = position;
            Color shade;

            for (int i = 0; i < menuItems.Count; i++)
            {
                if (i == selectedIndex) shade = selectedColor;
                else shade = unselectedColor;
                spriteBatch.DrawString(font, menuItems[i], location, shade);
                location.Y += font.LineSpacing + 5;
            }
        }
    }
}
