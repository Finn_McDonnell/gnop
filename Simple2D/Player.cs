﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Gnop
{
    public class Player : GameCharacter //Incorporates some unique features of the player controlled characters such as keyboard input.
    {
        //Members
        TimeSpan maxStamina;
        TimeSpan currentStamina;
        public Dictionary<string, Keys> playerKeyMap;
        

        //Constructors
        public Player(GraphicsDevice graphics, Texture2D texture, Single characterSpeed, int magazineSize, TimeSpan shotCoolDown, int wounds, TimeSpan maxStamina, bool obstructive)
            : base(graphics, texture, characterSpeed, magazineSize, shotCoolDown, wounds, obstructive)
        {
            initialiseBasics(maxStamina, characterSpeed);
            localAxes.Add(Vector2.UnitX);
            localAxes.Add(Vector2.UnitY);
        }
        
        public Player(GraphicsDevice graphics, Texture2D texture, Single characterSpeed, int magazineSize, TimeSpan shotCoolDown, int wounds, TimeSpan maxStamina, bool obstructive, Vector2 c1, Vector2 c2)
            : base(graphics, texture, characterSpeed, magazineSize, shotCoolDown, wounds, obstructive)
        {
            initialiseBasics(maxStamina, characterSpeed);
            localAxes.Add(c1);
            localAxes.Add(c2);
        }

        //Accessors
        public TimeSpan CurrentStamina
        {
            get
            {
                return currentStamina;
            }
            set
            {
                currentStamina = value;
            }
        }
        public TimeSpan MaxStamina
        {
            get
            {
                return maxStamina;
            }
        }

        //Methods
        void initialiseBasics(TimeSpan maxStamina, Single characterSpeed)
        {
            this.maxStamina = maxStamina;
            currentStamina = maxStamina;
            InitialSpeed = characterSpeed;
        }
        
        public void updatePlayerInput(bool left, bool right, bool up, bool down, bool turbo, bool shoot, GameTime gameTime) //Take input from game and update user controlled player
        {
            movementDirection = Vector2.Zero;
            if (left) movementDirection.X -= 1.0f;
            if (right) movementDirection.X += 1.0f;
            if (up) movementDirection.Y -= 1.0f;
            if (down) movementDirection.Y += 1.0f;
            if (turbo & currentStamina > TimeSpan.Zero)
            {
                speed = InitialSpeed * 3;
                currentStamina = TimeSpan.FromMilliseconds(currentStamina.Milliseconds - gameTime.ElapsedGameTime.Milliseconds);
            }
            else speed = InitialSpeed;
            if (!turbo & currentStamina < maxStamina) currentStamina = TimeSpan.FromMilliseconds(currentStamina.Milliseconds + gameTime.ElapsedGameTime.Milliseconds / 2);
            if (shoot) Shoot();
            UpdateVelocity();
        }

        public void wound(GameCharacter victim) //Inflict a wound on another character
        {
            victim.Wounded();
        }

        public override void Died() // Deactivate player if they have no wounds remaining
        {
            Active = false;
            wounds = 0;
        }
    }
}
