﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Gnop
{
    public class Projectile : GameObject
    {
        //Members
        //Constructors
        public Projectile(GraphicsDevice graphics, int textureX, int textureY, int width, int height, Single characterSpeed)
            : base(graphics, textureX, textureY, width, height, characterSpeed)
        {
            Active = false;
            UpdateVelocity();
        }
        //Accessors
        //Methods



    }
}
