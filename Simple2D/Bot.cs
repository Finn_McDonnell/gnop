﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Gnop
{
    public class Bot : GameCharacter //Incoporates unique handling for computer controlled characters
    {
        //Members

        //constructors
        public Bot(GraphicsDevice graphics, Texture2D texture, Single characterSpeed, int magazineSize, TimeSpan shotCoolDown, int wounds, bool obstructive)
            : base(graphics, texture, characterSpeed, magazineSize, shotCoolDown, wounds, obstructive)
        {
        }

        //Methods
        public void updateBotDirection(Vector2 point) //Update bots movement direction toward point
        {
            movementDirection = Vector2.Normalize(Vector2.Subtract(point, position));
        }
    }
}
