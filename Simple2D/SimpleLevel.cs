﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XMLLoader;

namespace Gnop
{
    public class SimpleLevel
    {
        //Members
        CharacterList enemies;
        BotRoster roster;
        bool initialized = false;
        Hashtable walls = new Hashtable(4);
        GameObject topWall;
        GameObject bottomWall;
        GameObject leftWall;
        GameObject rightWall;
        GameObject backDrop;

        //Constructors
        public SimpleLevel(CharacterList characters)
        {
            enemies = characters;
        }

        //Accessors
        public bool Initialized
        {
            get
            {
                return initialized;
            }
        }
        public BotRoster Roster
        {
            get
            {
                return roster;
            }
        }
        public Hashtable Walls
        {
            get
            {
                return walls;
            }
        }
        public GameObject BackDrop
        {
            get
            {
                return backDrop;
            }
        }

        //Methods
        public void InitialiseBots(GraphicsDevice graphics, Player protagonist, Hashtable textureList) //Bot configuration loaded from WaveX.xml
        {

            List<Bot> enemyList = new List<Bot>();

            foreach (Character character in enemies.Characters)
            {
                int magazineSize = character.MagazineSize;
                int toughness = character.Toughness;
                String textureName = character.TextureName;
                Single speed = character.Speed;
                TimeSpan shotCoolDown = TimeSpan.FromMilliseconds(character.ShotCoolDown);

                Bot enemy = new Bot(graphics, (Texture2D)textureList[textureName], speed, magazineSize, shotCoolDown, toughness, true);
                enemy.setStartPosition((int)character.StartPosition.X, (int)character.StartPosition.Y);
                enemy.setStartDirection((int)character.StartDirection.X, (int)character.StartDirection.Y);

                enemyList.Add(enemy);
            }
            roster = new BotRoster(enemyList);
            roster.ResetBotsToStart(protagonist);

            initialized = true;
        }

        public void InitialiseWalls(GraphicsDevice graphics, int windowHeight, int windowWidth)
        {
            topWall = new GameObject(graphics, 0, 0, 53, 63, 0.0f);
            bottomWall = new GameObject(graphics, 0, 0, 53, 63, 0.0f);
            leftWall = new GameObject(graphics, 0, 0, 53, 63, 0.0f);
            rightWall = new GameObject(graphics, 0, 0, 53, 63, 0.0f);
            backDrop = new GameObject(graphics, 0, 0, 66, 32, 0.0f);

            topWall.setPosition(0, 0);
            topWall.boundingBox.Width = windowWidth;
            topWall.boundingBox.Height = 40;

            bottomWall.setPosition(0, windowHeight - 40);
            bottomWall.boundingBox.Width = windowWidth;
            bottomWall.boundingBox.Height = 40;

            leftWall.setPosition(0, 0);
            leftWall.boundingBox.Width = 40;
            leftWall.boundingBox.Height = windowHeight;

            rightWall.setPosition(windowWidth - 40, 0);
            rightWall.boundingBox.Width = 40;
            rightWall.boundingBox.Height = windowHeight;

            backDrop.setPosition(0, 0);
            backDrop.boundingBox.Width = windowWidth;
            backDrop.boundingBox.Height = windowHeight;


            walls.Add("top", topWall);
            walls.Add("bottom", bottomWall);
            walls.Add("left", leftWall);
            walls.Add("right", rightWall);


            backDrop.updateBoundingBox();
            foreach (DictionaryEntry entry in walls)
            {
                GameObject wall = entry.Value as GameObject;
                wall.updateBoundingBox();
            }
        }
        public void RenderWalls(SpriteBatch spriteBatch)
        {
            backDrop.renderObject(spriteBatch);
            foreach (DictionaryEntry entry in walls)
            {
                GameObject wall = entry.Value as GameObject;
                wall.renderObject(spriteBatch);
            }
        }
    }
}
