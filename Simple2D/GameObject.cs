using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Gnop
{
    public class GameObject //Generic game object, most items (characters, obstacles, walls) in the game will inherit from this
    {
        //Members
        public Texture2D Texture;
        public Texture2D basicTexture;
        public Vector2 movementDirection = new Vector2(1.0f, 1.0f);
        public Vector2 Velocity = new Vector2(0.0f, 0.0f);
        public Vector2 facing = new Vector2(1.0f, 0.0f);
        public Single speed;
        public Single InitialSpeed;
        bool active = true;
        bool Obstructive = false;
        Single layer = 0.0f;

        protected Vector2 startPosition = Vector2.Zero;
        protected Vector2 startDirection = Vector2.Zero;
        public Vector2 position = Vector2.Zero;
        protected Rectangle textureLocation;
        protected Vector2 origin = Vector2.Zero;
        protected Single orientation = 0.0f;
        protected Vector2 scale = Vector2.One;
        public Rectangle boundingBox;

        //Constructors
        public GameObject(GraphicsDevice graphics, int textureX, int textureY, int width, int height, Single objectSpeed)
        {
            Texture = new Texture2D(graphics, width, height);
            basicTexture = new Texture2D(graphics, 1, 1);
            basicTexture.SetData(new[] { Color.White });
            textureLocation = new Rectangle(textureX, textureY, width, height);
            boundingBox = new Rectangle((int)position.X, (int)position.Y, width, width);
            scale = new Vector2(1.0f, (float)width / height);
            speed = objectSpeed;
            InitialSpeed = objectSpeed;
        }

        public GameObject(GraphicsDevice graphics, Texture2D texture, Single objectSpeed, bool obstructive) //This constructor scales an object to have height = width
        {
            Texture = texture;
            Obstructive = obstructive;
            basicTexture = new Texture2D(graphics, 1, 1);
            basicTexture.SetData(new[] { Color.White });
            boundingBox = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
            scale = new Vector2(1.0f, 1.0f);
            speed = objectSpeed;
            InitialSpeed = speed;
        }

        //Accessors
        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }

        public Single Layer
        {
            get
            {
                return layer;
            }
            set
            {
                layer = value;
            }
        }

        public Vector2 Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
            }
        }

        public void updatePosition(TimeSpan dt) //For moving objects like characters, update based on speed and direction
        {
            if (!Vector2.Equals(Velocity, Vector2.Zero))
            {
                position = Vector2.Add(position, Vector2.Multiply(Velocity, dt.Milliseconds));
            }
        }

        public void UpdateVelocity()
        {
            if (!Vector2.Equals(movementDirection, Vector2.Zero))
            {
                Velocity.X = Vector2.Normalize(movementDirection).X * speed;
                Velocity.Y = Vector2.Normalize(movementDirection).Y * speed;
            }
            else 
            {
                Velocity.X = 0;
                Velocity.Y = 0;
            }
        }

        public void Accelerate(Vector2 acceleration, TimeSpan dt)
        {
            Velocity = Vector2.Add(Velocity, Vector2.Multiply(acceleration, dt.Milliseconds));
            UpdateMovementDirection();
        }

        public void UpdateMovementDirection()
        {
            if (!Vector2.Equals(Velocity, Vector2.Zero))
            {
                movementDirection = Vector2.Normalize(Velocity);
            }
        }

        public virtual void setStartPosition(int X, int Y) //Initialise objects starting location
        {
            startPosition.X = X;
            startPosition.Y = Y;
        }

        public virtual void setStartDirection(int X, int Y) //Used to initialize character to its start position
        {
            startDirection.X = X;
            startDirection.Y = Y;
        }

        public void setPosition(int X, int Y) //Set objects current location
        {
            position.X = X;
            position.Y = Y;
        }

        public void setMovementDirection(int X, int Y) //Set objects current movement direction
        {
            movementDirection.X = X;
            movementDirection.Y = Y;
        }

        public void setOrigin(int X, int Y) //set origin for objects that will be rotated
        {
            origin.X = X;
            origin.Y = Y;
        }

        public void setScaling(float width, float height)
        {
            scale = new Vector2(width, height);
        }

        public virtual void updateBoundingBox() //Maintain bounding box rectangle, no special handling
        {
            boundingBox.X = (int)position.X;
            boundingBox.Y = (int)position.Y;
        }

        public void renderObjectByScale(SpriteBatch spriteBatch) //Draw object stretched by its scale factor
        {
            spriteBatch.Draw(Texture, position, null, Color.White, orientation, origin, scale, SpriteEffects.None, layer);
            //spriteBatch.Draw(basicTexture, boundingBox, Color.Black);
        }

        public void renderObject(SpriteBatch spriteBatch) //Draw object in its bounding box, this will tile (LinearWrap) if the texturefile is smaller that destination rectangle
        {
            spriteBatch.Draw(Texture, boundingBox, boundingBox, Color.White, orientation, origin, SpriteEffects.None, layer);
            //spriteBatch.Draw(basicTexture, boundingBox, Color.Black);
        }
    }
}
