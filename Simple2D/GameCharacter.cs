﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Gnop
{
    public class GameCharacter : GameObject // general class for characters in game. Computer controlled (Bots) and player controlled (Players) derive from this.
    {
        //Members
        List<Projectile> magazine;
        public List<Vector2> localAxes = new List<Vector2>(2);

        protected int toughness;
        protected int wounds;
        TimeSpan coolDown;
        TimeSpan lastShot = TimeSpan.Zero;

        //Constructors
        public GameCharacter(GraphicsDevice graphics, Texture2D texture, Single characterSpeed, int magazineSize, TimeSpan shotCoolDown, int wounds, bool obstructive)
            : base(graphics, texture, characterSpeed, obstructive)
        {
            coolDown = shotCoolDown;
            this.toughness = wounds;
            this.wounds = wounds;
            magazine = new List<Projectile>(magazineSize);
            for (int i = 0; i < magazineSize; i++)
            {
                Projectile bullet = new Projectile(graphics, 0, 0, 5, 5, 0.15f);
                magazine.Add(bullet);
            }
            this.Texture = texture;
            this.updateOrigin();
        }

        //Accessors
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }
        public Vector2 StartPosition
        {
            get
            {
                return startPosition;
            }
        }
        public Vector2 Origin
        {
            get
            {
                return origin;
            }
        }

        public List<Projectile> Magazine
        {
            get
            {
                return magazine;
            }
        }

        public int Toughness
        {
            get
            {
                return wounds;
            }
        }

        //Methods
        public void updateOrigin() //For rotating sprites the origin needs to be set centrally
        {
            origin.X = (int)Texture.Width / 2;
            origin.Y = (int)Texture.Height / 2;
        }

        public virtual void Update(TimeSpan dt) //Call update methods for active characters
        {
            if (Active)
            {
                UpdateVelocity();
                updatePosition(dt);
                updateBoundingBox();
            }
            updateBullets(dt);
        }

        protected void updateBullets(TimeSpan dt) //Update characters fired shots
        {
            lastShot += dt;
            foreach (Projectile bullet in magazine)
            {
                if (bullet.Active)
                {
                    bullet.updatePosition(dt);
                    bullet.updateBoundingBox();
                }
            }
        }

        public void resetToStart() //Reset character to starting location
        {
            position.X = startPosition.X;
            position.Y = startPosition.Y;
            movementDirection.X = startDirection.X;
            movementDirection.Y = startDirection.Y;
            UpdateVelocity();

            updateBoundingBox();

            foreach (Projectile bullet in magazine)
            {
                bullet.Active = false;
            }

            speed = InitialSpeed;
            wounds = toughness;
            Active = true;
        }

        public override void setStartPosition(int X, int Y) //Used to initialize character to its start position
        {
            startPosition.X = X;
            startPosition.Y = Y;
            resetToStart();
        }

        public override void setStartDirection(int X, int Y) //Used to initialize character to its start position
        {
            startDirection.X = X;
            startDirection.Y = Y;
            resetToStart();
        }

        public void faceMovementDirection() //Change the characters orientation to match their velocity
        {
            if (!Vector2.Equals(movementDirection, Vector2.Zero))
            {
                facePoint(Vector2.Add(position, movementDirection));
            }
        }

        public void facePoint(Vector2 point) //Change the orientation of a character to face a given point defined by a vector
        {
            Vector2 relativeVector = Vector2.Subtract(point, position);
            if (relativeVector.Equals(Vector2.Zero))
            {
                orientation = 0.0f;
            }
            else
            {
                Single dotProduct = Vector2.Dot(relativeVector, Vector2.Negate(Vector2.UnitY));
                Single magnitude = Vector2.Distance(Vector2.Zero, relativeVector);
                orientation = (Single)Math.Acos(dotProduct / magnitude);
                if (relativeVector.X < 0) orientation *= -1;
            }
            UpdateFacing();
        }

        public void facePoint(int X, int Y) //Change the orientation of a character to face a given point defined by coordinates
        {
            Vector2 point;
            point.X = X;
            point.Y = Y;
            facePoint(point);
            UpdateFacing();
        }

        public void UpdateFacing() //Maintain orientation as a vector to determine shot direction
        {
            facing.X = movementDirection.X;
            facing.Y = movementDirection.Y;
            updateBoundingBox();
        }

        public new void updateBoundingBox() //Maintain bounding box for collisions and drawing
        {
            boundingBox.X = (int)(position.X);
            boundingBox.Y = (int)(position.Y);
            boundingBox.Offset((int)(-boundingBox.Width / 2), (int)(-boundingBox.Height / 2));
        }

        public void calculateLocalAxes()
        {
            localAxes.Clear();
            updateOrigin();
            localAxes.Add(new Vector2(origin.X, -origin.Y));
            localAxes.Add(new Vector2(origin.X, origin.Y));
        }

        public void Shoot() //Shoot handler, activates a shotif allowed
        {
            if (lastShot >= coolDown)
            {
                foreach (Projectile bullet in magazine)
                {
                    if (!bullet.Active)
                    {
                        activateBullet(bullet);
                        lastShot = TimeSpan.Zero;
                        break;
                    }
                }
            }
        }

        public void Wounded() //Wound character
        {
            if (wounds <= 1) Died();
            else --wounds;
        }

        public virtual void Died() //Deactivate character if they have reached 0 wounds
        {
            Active = false;
            wounds = 0;
            position = Vector2.Zero;
            speed = 0.0f;
            updateBoundingBox();
        }

        public void activateBullet(Projectile bullet) //Re initialise inactive shot 
        {
            bullet.setPosition((int)position.X, (int)position.Y);
            bullet.movementDirection = facing;
            bullet.UpdateVelocity();
            bullet.updateBoundingBox();
            bullet.Active = true;
        }

        public void DrawCharacter(SpriteBatch spriteBatch) //Draw character
        {
            if (Active) renderObjectByScale(spriteBatch);
            DrawBullets(spriteBatch);
        }

        public void DrawBullets(SpriteBatch spriteBatch) //Draw Characters active shots
        {
            foreach (Projectile bullet in magazine)
            {
                if (bullet.Active)
                {
                    bullet.renderObject(spriteBatch);
                }
            }
        }
    }
}
